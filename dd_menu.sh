#!/usr/bin/env bash


##############################
# Creator : Alejka
# Porpuse : dd Menu
# Date : 20/10/2020
# Version : 1.0.0
##############################

backup_zip_disk(){
  read -p "Please provide the disk which you want to backup in zip : " disk_name
  if [[ -z $disk_name ]] ; then
  help
  else
  dd if=$disk_name of=/home/backup_zip_disk_$(date +%Y\%m\%d) && tar cfvz /home/backup_zip_disk_$(date +%Y\%m\%d).tar.gz /home &&
  echo "!!!SUCCSESS!!! Backup is zipped and saved in /home folder as backup_disk_.tar.gz"
  sleep 4
  fi
}


clean_swap_part(){
    echo clean_swap_part
}


backup_zip_root_part(){
    echo backup_zip_root_part
}



backup_zip_home_part(){
  read -p "Please provide the disk partition /home which you want to backup in zip : " part_name
  if [[ -z $part_name ]] ; then
  help
  else
  dd if=$part_name of=/home/backup_home_part_$(date +%Y\%m\%d) && tar cfvz /home/backup_home_part_$(date +%Y\%m\%d).tar.gz /home &&
  echo "!!!SUCCSESS!!! Backup is zipped and saved in /home folder as backup_home_part.tar.gz"
  sleep 4
  fi
}


backup_home_part(){
    read -p "Please provide the disk partition where it mounts to /home , backup will be saved in /home folder.: " part_name
    if [[ -z $part_name ]] ; then
    help
    else
    dd if=$part_name of=/home/backup_home_part_$(date +%Y\%m\%d).img &&
    echo "!!!SUCCSESS!!! Backup is saved in /home folder as backup_home_part.img"
    sleep 4
    fi
}


backup_mbr(){

  read -p "Please provide the disk name you want to backup in your home Folder: " disk_name

  if [[ -z $disk_name ]] ; then
  help
  else
  dd if=$disk_name of=/home/mbr_$(date +%Y\%m\%d).img count=2047 bs=1 &&
  echo "!!SUCCSSES!! The backup is saved as mbr_date file in /home folder ."
  sleep 4
  fi
}


  deco(){
      _time=2.5
      l="###############################"
      printf "\n$l\n # %s\n$l\n" "$@"
      sleep $_time
      clear
}



  help(){
    deco "Incorrect use of script"
    exit 1

  }

PS3='Backup menu -> Choose your Option: '
options=("Backup mbr" "Backup Home Partition" "Backup and zip home partition" "Backup and zip root partition" "Clean swap partition" "Backup and zip disk" "Quit")
select fav in "${options[@]}"; do
    case $fav in
        "Backup mbr")
            echo "You selected $fav -> backing up your mbr" ;
            backup_mbr
            ;;
        "Backup Home Partition")
            echo "You selected $fav -> Backing up home partition"
	          backup_home_part
            ;;
        "Backup and zip home partition")
            echo "You selected $fav -> backing up and zipping home partition"
	          backup_zip_home_part
            ;;
	      "Backup and zip root partition")
             echo "You selected $fav -> Backing up and zipping root partition"
           # backup_zip_root_part
            ;;
         "Clean swap partition")
             echo "You selected $fav -> Cleaning swap partition"
    #  clean_swap_part
            ;;
         "Backup and zip disk")
             echo "You selected $fav -> Backing up and zipping disk"
             backup_zip_disk
            ;;

	"Quit")
	    echo "You requested to exit"
	    exit
	    ;;
        *) echo "invalid option $REPLY";;
    esac
done
